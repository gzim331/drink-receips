import json
from time import time

dataFilename = "data.json"


def checkPossibleDrinks(inputElements):
    possibleDrinks = []

    with open(dataFilename) as json_file:
        drinks = json.loads(json_file.read())

        for drink in drinks['drinks']:
            myIngredients = []
            ingredientOccur = False
            for ingredient in drink['ingredients']:
                for element in inputElements:
                    if element == ingredient:
                        myIngredients.append(element)
                        ingredientOccur = True
            if ingredientOccur == True:
                possibleDrinks.append([
                    drink['id'],
                    drink['name'],
                    len(myIngredients),
                    len(drink['ingredients']),
                    (len(myIngredients) / len(drink['ingredients'])) * 100
                ])

    return possibleDrinks


def showIngredientsList():
    with open(dataFilename) as json_file:
        drinks = json.loads(json_file.read())
        ingredientsList = []
        for drink in drinks['drinks']:
            for ingredient in drink['ingredients']:
                ingredientsList.append(ingredient)
        return sorted(list(set(ingredientsList)))


def getDrinksFromFile():
    file = open(dataFilename, "r")
    return json.loads(file.read())


def getAllDrinks():
    with open(dataFilename) as json_file:
        drinks = json.loads(json_file.read())
        allDrinksList = []
        for drink in drinks['drinks']:
            allDrinksList.append(drink['name'])
        return sorted(list(set(allDrinksList)))


def findInfoAboutDrinks(name):
    with open(dataFilename) as json_file:
        returnDrinksArray = []
        drinks = json.loads(json_file.read())
        for drink in drinks['drinks']:
            if drink['name'] == name:
                returnDrinksArray.append(drink)
            else:
                continue
        return returnDrinksArray


def printInfoAboutDrinks(drinks):
    for drink in drinks:
        print('\n--------------------------------------')
        print('NAME: ', drink['name'])
        print('ID: ', drink['id'])
        print('INGREDIENTS:')
        for ingredient in drink['ingredients']:
            print(' ', ingredient)
        print('RECIPE:\n', drink['recipe'])
        print('--------------------------------------\n')


def saveDrinkInFile(name, ingredients, recipe):
    with open(dataFilename) as json_file:
        data = json.load(json_file)
        temp = data['drinks']
        newDrink = {
            "id": int(time()),
            "name": name,
            "ingredients": ingredients,
            "recipe": recipe
        }
        temp.append(newDrink)
        write_json(data)


def write_json(data):
    with open(dataFilename, 'w') as f:
        json.dump(data, f, indent=2)
