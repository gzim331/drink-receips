from Switcher import *

s = Switcher()
firstIteration = True
while True:
    if firstIteration == True:
        s.help()
        firstIteration = False

    try:
        operation = input("Operation: ")
        if operation == 'esc':
            break
        s.indirect(operation)
    except Exception as e:
        print(e)
