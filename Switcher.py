from service import *

inputElements = []


class Switcher(object):
    def indirect(self, operation):
        method = getattr(self, operation, lambda: 'Invalid')
        return method()

    def help(self):
        print('- help\n- add\n- drinks\n- ingredients\n- list\n- info\n- esc')

    def add(self):
        ingredients = []
        loopIngredients = True
        ingredientIterate = 1
        name = input('name: ')
        print('Confirm each ingredient with <enter>. To finish, press enter or type esc')
        while loopIngredients:
            ingredientInput = input(f'Ingredient {ingredientIterate}: ')
            if ingredientInput == 'esc' or ingredientInput == '':
                loopIngredients = False
            else:
                ingredients.append(ingredientInput)
                ingredientIterate += 1
        recipe = input('recipe: ')
        saveDrinkInFile(name, ingredients, recipe)

    def drinks(self):
        print('Confirm each ingredient with <enter>. To finish, press enter or type esc')
        loopData = True
        while loopData:
            elementInput = input('Ingredient which you have: ')
            if elementInput == 'esc' or elementInput == '':
                loopData = False
            elif elementInput == '?':
                self.ingredients()
            else:
                inputElements.append(elementInput)

        # name, the number of ingredients i have, number of ingredients in drink, % of ingredients owned
        for drink in checkPossibleDrinks(inputElements):
            print("%d. %s %d/%d - %.0f%%" % (drink[0], drink[1], drink[2], drink[3], drink[4]))

    def ingredients(self):
        for ingredient in showIngredientsList():
            print(ingredient)

    def list(self):
        for drink in getAllDrinks():
            print(drink)

    def info(self):
        loopFind = True
        while loopFind:
            drinkNameInput = input('Drink name: ')
            drinks = findInfoAboutDrinks(drinkNameInput)
            if drinks == None:
                print('Drink not found')
            elif drinkNameInput == 'esc':
                loopFind = False
            else:
                loopFind = False
                printInfoAboutDrinks(drinks)

    def esc(self):
        return 'esc'

    def __getattr__(self, item):
        print('Undefined command')
